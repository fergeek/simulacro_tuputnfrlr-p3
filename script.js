const $ = x => document.querySelector(x);
const token = 'MM_yrTDHTF-VlGxnHied';
const apiRequest = (type, data = []) => {
  if (['character', 'book'].some(x => x == type)){
    let dir = `https://the-one-api.dev/v2/${type}/${data.join('/')}`;
    let conf = {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`
      }
    }
    return fetch(dir, conf);
  } else {
    return null;
  }
};
const itemChar = obj => {
    let element = document.createElement('option');
    // Human | Belemir => Sexo Raza | Nombre
    element.innerHTML = `${obj.gender == 'Female' ? '♀' : obj.gender == 'Male' ? '♂' : '⚥'} ${obj.race} | ${obj.name}`;
    element.value = obj.wikiUrl;
    return element;
};
const itemBook = (obj, i) => {
    let element = document.createElement('option');
    // 1. The Breaking of the Fellowship | Orden. Nombre
    element.innerHTML = `${i+1}. ${obj.name}`;
    element.value = obj._id;
    return element;
};
const iniMenu = query => {
  let options = $(query).querySelectorAll('li');
  options.forEach(opt => {
    opt.addEventListener('click', e => {
      options.forEach(x => {
        let box = $(`#box_${x.id.split('_')[1]}`) || false;
        if(x !== e.target){
          x.classList.remove('active');
          box && box.classList.add('disabled');
        } else {
          e.target.classList.add('active');
          box && box.classList.remove('disabled')
        }
      });
    });
  });
}
